function refreshLedger() {
  var memberVat = document.getElementById("member-vat").textContent;
  var request = new XMLHttpRequest();
  request.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      getLedgerInfo();
    }
  });
  request.open("GET", "/cgi-bin/fetchExternal?u=" + memberVat, true);
  request.send();
}
function getMemberInfo() {
  getLedgerInfo();
  getMemberServices();
}
function getLedgerInfo() {
  var memberVat = document.getElementById("member-vat").textContent;
  var request = new XMLHttpRequest();
  request.open("GET", "/cgi-bin/memberLedger?m=" + memberVat, true);
  request.timeout = 5000;
  request.send();
  request.onload = function getLedger() {
    let ledgerTable = document.getElementById("ledger");
    ledgerTable.innerHTML = "";
    ledgerTable.appendChild(parseTable(this.responseText));
  };
}
function getMemberServices() {
  var memberVat = document.getElementById("member-vat").textContent;
  var request = new XMLHttpRequest();
  request.onload = function getServices() {
    const services = JSON.parse(this.responseText);
    const servicesDiv = document.getElementById("connected-services");
    servicesDiv.innerHTML = "";
    services.forEach(({ service, uri }) => {
      servicesDiv.insertAdjacentHTML(
        "beforeend",
        `<div class="member-info"><h3>${service}</h3><p>${uri}</p></div>`
      );
    });
  };
  request.open("GET", "/cgi-bin/memberServices?u=" + memberVat, true);
  request.timeout = 5000;
  request.send();
}
function parseTable(tableContent) {
  const table = document.createElement("table");
  table.classList.add("ledger-table");
  const tableLines = tableContent.trim().split("\n");
  const tableHeader = tableLines.shift().split(";");

  console.log(tableLines);
  let headerElement = document.createElement("tr");
  tableHeader.map((field) => {
    let headerField = document.createElement("th");
    headerField.innerText = field;
    headerElement.appendChild(headerField);
  });
  table.appendChild(headerElement);

  tableLines.map((line) => {
    let rowElement = document.createElement("tr");
    line.split(";").map((field) => {
      let rowField = document.createElement("td");
      rowField.innerText = field.replace(/^"(.*)"$/, "$1");
      rowElement.appendChild(rowField);
    });
    table.appendChild(rowElement);
  });
  return table;
}
function toggleMemberServices() {
  const services = document.getElementById("connected-services");
  if (services.style.display === "flex") {
    services.style.display = "none";
  } else {
    services.style.display = "flex";
  }
}
