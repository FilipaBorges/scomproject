# SCOMRED Group Project - Group One

This repository includes files to comply to proposed project of SCOMRED, SWitCH 2020/21.

Clone this repository to the virtual server provided by ISEP that will serve the specific Mock/Prototype Application.

You might need to configure git in your virtual server:

```
git config --global -e
```

The structure should be:

```
.
├── README.md (this file)
├── docs
│   └── SWS01-Add-Family-Member.md
│   (Documentation folder)
├── e-fatura
│   └── (Files for e-fatura User Stories)
├── financial-provider
│   └── (Files for financial provider User Stories)
├── public-utility
│   └── (Files for public utility User Stories)
└── sws
    └──  (Files for SWS User Stories)
```

As explained in the classes, using Apache server, files to be presented as html pages should be inside `/var/www/html/` folder, and script files to be used as API providers should be inside `var/www/cgi-bin/`.

In order to use Git magic to improve cooperation, one could create symbolic links to files inside this repository:

```
# Don't forget to add permissions to this folder,
# in order to use with Apache server:
~$ cd /
/$ chmod o+x /root /root/scomproject /root/scomproject/sws
/$
# Apache needs execute rights in order to get working symlinks
~$ cd ~/scomproject/ #cd to this repo
~/scomproject$
~/scomproject$ ls sws/
add-family-member.html  familyMembers  familyMembers.js  sws-family.html
~/scomproject$
# Let's pretend we want to symlink sws-family.html:
~/scomproject$
~/scomproject$ cd /var/www/html
/var/www/html$ ln -s ~/scomproject/sws/sws-family.html
/var/www/html$
# Let's pretend we want to symlink familyMembers (a bash script):
/var/www/html$ cd ../cgi-bin
/var/www/html$ ln -s ~/scomproject/sws/familyMembers
# Don't forget to give execute permissions to bash scripts (in root folder):
/var/www/html$ cd ~/scomproject/<app/mock folder>
/scomproject/sws/$ chmod +x familyMembers
```

Now you can create branches to implement your User Stories, use Pull Requests.
You can test branches remotely by checking to specific branches, without messing with `master` files.
You can update remote files by issuing a git pull.
