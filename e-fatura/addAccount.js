function addAccount(e) {
    e.preventDefault();
    let vatNumber = document.getElementById("VAT-number").value;
    let personName = document.getElementById("person-name").value;

    if (validateName(personName) && validateVAT(vatNumber)) {
        const formData = `personName=${personName}&vatNumber=${vatNumber}`;
        const request = new XMLHttpRequest();
        let responseText;
        request.onreadystatechange = function getResponse() {
            if (this.readyState === 4 && this.status === 200) {
                responseText = "The account was successfully created!";
                document.getElementById("response").innerHTML =  "";
                document.getElementById("addAccountForm").innerHTML = responseText;
            }
            if (this.readyState === 4 && this.status === 403) {
                responseText = "The account already exists!";
                document.getElementById("response").innerHTML = responseText;
            }
            if (this.readyState === 4 && this.status === 400) {
                responseText = "Invalid request";
                document.getElementById("response").innerHTML = responseText;
            }
        };
        request.open("POST", "/cgi-bin/addAccount");
        request.send(formData);
    }
}

function validateName(personName){
    if (personName === null || personName.length === 0 || checkNameFormat(personName) === false){
        alert("The name is not valid");
        return false;
    }
    return true;
}

function validateVAT(vatNumber){
    if (vatNumber === null || vatNumber.length !== 9 || checkVatFormat(vatNumber) === false){
        alert("The vat number is not valid");
        return false;
    }
    return true;
}

function checkNameFormat(name) {
    let nameFormat = /^[a-zA-ZÀ-ÿ-, ]+$/;
    let result = true;

    if (nameFormat.test(name) === false) {
        result = false;
    }
    return result;
}

function checkVatFormat(vat) {
    let vatFormat = /^[123][0-9]{8}$/;
    let result = true;

    if (vatFormat.test(vat) === false) {
        result = false;
    }
    return result;
}
