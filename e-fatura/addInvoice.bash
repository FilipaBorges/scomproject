#!/bin/bash
ACCOUNT_LIST_FOLDER="/var/www/cgi-bin/e-fatura-accounts"

error_response() {
  echo "Status: 400 Bad Request"
  echo ""
  echo "Error: $1"
  exit
}

if [ "$CONTENT_LENGTH" == 0 ]; then
  error_response "No content found on the request"
fi

if [ "$REQUEST_METHOD" != "POST" ]; then
  error_response "Invalid method. The only method supported is POST"
fi

read -n $CONTENT_LENGTH QUERY_STRING_POST

INVOICE_DATE=${QUERY_STRING_POST#invoice-date=}
INVOICE_DATE=${INVOICE_DATE%%&*}
DESCRIPTION=${QUERY_STRING_POST#*description=}
DESCRIPTION=${DESCRIPTION%%&*}
DESCRIPTION=\"$DESCRIPTION\"
INVOICE_NUMBER=${QUERY_STRING_POST#*invoice-number=}
INVOICE_NUMBER=${INVOICE_NUMBER%%&*}
CONSUMER_VAT=${QUERY_STRING_POST#*consumer-VAT=}
CONSUMER_VAT=${CONSUMER_VAT%%&*}
PROVIDER_VAT=${QUERY_STRING_POST#*provider-VAT=}
PROVIDER_VAT=${PROVIDER_VAT%%&*}
AMOUNT=${QUERY_STRING_POST#*invoice-amount=}
AMOUNT=${AMOUNT%&*}
CURRENCY=${QUERY_STRING_POST#*invoice-currency=}
CURRENCY=${CURRENCY%&*}
CURRENCY=\"$CURRENCY\"

if [ -z "$INVOICE_DATE" ]; then
	error_response "The invoice has to have a date"
fi

if [ -z "$DESCRIPTION" ]; then
	error_response "The invoice cannot be empty"
fi

if [ -z  "$INVOICE_NUMBER" ]; then
	error_response "The invoice number cannot be empty"
fi

if [ -z "$CONSUMER_VAT" ]; then
	error_response "The consumer VAT number cannot be empty"
fi

if [ -z "$PROVIDER_VAT" ]; then
	error_response "The provider VAT number cannot be empty"
fi

if [ -z "$AMOUNT" ]; then
	error_response "The amount cannot be empty"
fi

if [ -z "$CURRENCY" ]; then
	error_response "The currency cannot be empty"
fi

CONSUMER_VAT_FOLDER="$ACCOUNT_LIST_FOLDER/$CONSUMER_VAT"
INVOICES_FOLDER="$CONSUMER_VAT_FOLDER/INVOICES"

if [ -d "$CONSUMER_VAT_FOLDER" ]; then

	if [ ! -d "$INVOICES_FOLDER" ]; then

		mkdir -p "$CONSUMER_VAT_FOLDER/INVOICES"
	fi

	INVOICE_FILE="$INVOICES_FOLDER/$INVOICE_NUMBER"

	if [ ! -f "$INVOICE_FILE" ]; then

		echo "Invoice Date; Description; Invoice Number; Consumer VAT Number; Provider VAT Number; Amount; Currency" > "$INVOICE_FILE"
		echo "$INVOICE_DATE;$DESCRIPTION;$INVOICE_NUMBER;$CONSUMER_VAT;$PROVIDER_VAT;$AMOUNT;$CURRENCY" >> "$INVOICE_FILE"

		echo "Status: 200"
		echo ""
		exit
	fi

	error_response "The invoice already exists!"

fi

echo "Status: 403"
echo ""
echo "Account for the consumer VAT number does not exist. Please create a new account"
